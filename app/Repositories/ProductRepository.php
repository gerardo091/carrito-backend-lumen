<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Interfaces\IProductRepository;
use Illuminate\Pagination\Paginator;

class ProductRepository implements IProductRepository
{

    public function paginate(int $take): Paginator
    {
        return Product::simplePaginate($take);

    }

    public function find(int $id) : ?Product
    {
        return Product::find($id);
    }

    public function store($store)
    {
    }

    public function update($store)
    {
    }

    public function destroy(int $id)
    {
    }
}
