<?php

namespace App\Http\Controllers;

use GrahamCampbell\ResultType\Result;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\IProductRepository;

class ProductController extends Controller
{
    private IProductRepository $productRepository;

    public function __construct(IProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(){

        return $this->productRepository->paginate(10);
    }

    public function show(int $id){
        $result =  $this->productRepository->find($id);
        if($result){
            return $result;
        }

        return response('Prduct not found', 404);
    }

    public function store(){

    }

    public function update($id){

        
    }

    public function destroy($id){

    }
    
}
