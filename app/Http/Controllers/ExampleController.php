<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\IExampleRepository;
use GrahamCampbell\ResultType\Result;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    private IExampleRepository $exampleRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IExampleRepository $exampleRepository)
    {
        $this->exampleRepository = $exampleRepository;
    }

    //
    public function index()
    {
        return $this->exampleRepository->getAll();
    }
}
